const Joi = require('joi');

const axios = require('axios')

let BaseUrl = 'https://devnet-gate.decimalchain.com'

const Api = {
    getBlockPrice() {
        return axios.create({
            withCredentials: true,
            baseURL: BaseUrl
        })
            .get('/api/blocks?limit=1&offset=0')
            .then((response) => {
                return response.data.result.blocks[0].emission
            })
    }
}

async function response(request) {
    let result = []
    let BlockPrice = 0
    switch (request.payload.network) {
        case 'devnet': {
            BaseUrl = 'https://devnet-gate.decimalchain.com'
            break
        }
        case 'testnet': {
            BaseUrl = 'https://testnet-gate.decimalchain.com'
            break
        }
        case 'mainnet': {
            BaseUrl = 'https://mainnet-gate.decimalchain.com'
        }
    }
    await Api.getBlockPrice().then((response) => {
        BlockPrice = Number(response)
    });

    request.payload.validators.map(async (p) => {
        let res = ((BlockPrice * 15710 * 0.9) / Number(p.stakeV)) * Number(p.stakeD) * (1 - Number(p.commissionV) / 100) / 1000000000000000000
        result.push({validatorId: p.validatorId, delegatorIncome: res})
    })
    return {
        data: result
    };
}

const responseScheme = Joi.object({
    data: Joi.array().items(
        Joi.object({
            validatorId: Joi.string().required().example('dxvaloper1mvqrrrlcd0gdt256jxg7n68e4neppu5tk872z3'),
            delegatorIncome: Joi.number().required().example(12.15651),
        })
    )
});

module.exports = {
    method: 'POST',
    path: '/calculate',
    options: {
        handler: response,
        tags: ['api'],
        validate: {
            payload: {
                network: Joi.string().min(1).max(100).required().example('devnet'),
                validators: Joi.array().items(Joi.object({
                        validatorId: Joi.string().required().example('dxvaloper1mvqrrrlcd0gdt256jxg7n68e4neppu5tk872z3'),
                        stakeD: Joi.number().required().example(21.1),
                        stakeV: Joi.number().required().example(41.3),
                        commissionV: Joi.number().required().example(10.1)
                    }
                ))
            }
        },
        response: {schema: responseScheme}
    }
};
